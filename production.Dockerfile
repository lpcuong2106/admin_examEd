FROM node:12.16.1-alpine3.11 as builder

WORKDIR /usr/app

COPY ["package.json", "package-lock.json", "./"]

RUN npm install -g pm2
RUN npm install --silent

COPY . ./

RUN npm run build


# Mo port trong container de cac container lien lac nhau
EXPOSE 3000 

#run comand moi khi start
CMD ["pm2-runtime", "npm", "--", "start"]


