export interface RoutersType {
  path: string;
  breadcrumb: string | object;
}
