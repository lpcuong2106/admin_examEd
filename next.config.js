const withSass = require("@zeit/next-sass");
const withCSS = require("@zeit/next-css");
const isProd = process.env.NODE_ENV === "production";
module.exports = withSass(
  withCSS({
    poweredByHeader: false,
    cssModules: true,
    cssLoaderOptions: {
      importLoaders: 1,
      localIdentName: isProd ? "[hash:base64:8]" : "[local]__[hash:base64:8]",
    },
    webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
      // Note: we provide webpack above so you should not `require` it
      // Perform customizations to webpack config
      // Important: return the modified config
      if (dev) {
        config.devtool = "source-map";
      }
      config.module.rules.push({
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        use: {
          loader: "url-loader",
          options: {
            limit: 100000,
          },
        },
      });
      return config;
    },
  })
);
