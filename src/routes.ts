export default [
  { path: "/create-course", breadcrumb: "Pizzas" },
  { path: "/create-course/:pizzaId", breadcrumb: "Edit Pizza" },
  { path: "/pizza/:pizzaId/toppings", breadcrumb: "Pizza Topping" },
  { path: "/another", breadcrumb: "Another" },
  { path: "/one", breadcrumb: "One" }
];
