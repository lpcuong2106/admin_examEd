import routes from "./routes";
interface RouteType {
  name: string;
  path: string;
}
test("correct routes are exported", () => {
  expect(
    routes.map(({ name, path }: RouteType) => {
      `${name}: ${path}`;
    })
  ).toMatchSnapshot();
});
