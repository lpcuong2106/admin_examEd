import Document, { Html, Head, Main, NextScript } from "next/document";
import React from "react";

class MyDocument<NextPage> extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Head>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
          />
          <meta
            name="description"
            content="Start your development with a Dashboard for Bootstrap 4."
          />
          <meta name="author" content="Creative Tim" />
          <link rel="icon" href="/img/brand/favicon.png" type="image/png" />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
          />
          <link
            rel="stylesheet"
            href="/vendor/nucleo/css/nucleo.css"
            type="text/css"
          />
          <link
            rel="stylesheet"
            href="/vendor/@fortawesome/fontawesome-free/css/all.min.css"
            type="text/css"
          />
          <link
            rel="stylesheet"
            href="/css/argon.css?v=1.1.0"
            type="text/css"
          />
        </Head>
        <body className={"g-sidenav-show g-sidenav-pinned"}>
          <Main />
          <NextScript />
          <script src="/vendor/jquery/dist/jquery.min.js"></script>
          <script src="/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
          <script src="/vendor/js-cookie/js.cookie.js"></script>
          <script src="/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
          <script src="/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
          <script src="/vendor/chart.js/dist/Chart.min.js"></script>
          <script src="/vendor/chart.js/dist/Chart.extension.js"></script>
          <script src="/js/argon.js?v=1.1.0"></script>
          <script src="/js/demo.min.js"></script>
        </body>
      </Html>
    );
  }
}

export default MyDocument;
