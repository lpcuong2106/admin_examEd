import React from "react";
import Head from "next/head";
import TopNav from "../../layouts/TopNav";
import Breadcrumb from "../../layouts/Breadcrumb";
import { gql } from "apollo-boost";
import { useMutation } from "@apollo/react-hooks";
import { Form } from "react-bootstrap";
import { Formik } from "formik";
import * as yup from "yup";
const CREATE_COURSE = gql`
  mutation createCouse($title: String!) {
    createCourse(input: { title: $title }) {
      title
      urlLogo
    }
  }
`;

const CreateCourse = () => {
  const [addCourse, { data }] = useMutation(CREATE_COURSE);
  // const handleSubmit = (event) => {
  //   event.preventDefault();
  //   console.log(event.currentTarget);
  // };
  const schema = yup.object({
    title: yup.string().required(),
  });
  console.log(data);
  return (
    <React.Fragment>
      <Head>
        <title>Tạo khóa học - YC Exam Online</title>
      </Head>
      <div className="main-content" id="panel">
        <TopNav />
        <div className="header bg-primary pb-6">
          <div className="container-fluid">
            <div className="header-body">
              <Breadcrumb />
            </div>
          </div>
        </div>
        <div className="container-fluid mt--6">
          <div className="row">
            <div className="col-lg-12">
              <div className="card-wrapper">
                <div className="card">
                  <div className="card-header">
                    <h3 className="mb-0">Nhập tên khóa học</h3>
                  </div>
                  <div className="card-body">
                    <Formik
                      validationSchema={schema}
                      onSubmit={(values, { setSubmitting }) => {
                        console.log(values);
                        addCourse({ variables: values });
                      }}
                      initialValues={{ title: "" }}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                      }) => (
                        <Form onSubmit={handleSubmit}>
                          <Form.Group controlId="formBasic">
                            <Form.Label>Tên khóa học</Form.Label>
                            <Form.Control
                              type="text"
                              name="title"
                              placeholder="Nhap ten title"
                              value={values.title}
                              onChange={handleChange}
                              isValid={touched.title && !errors.title}
                            />
                            {errors.title && touched.title && errors.title}
                          </Form.Group>

                          <button
                            type="submit"
                            className={"btn btn-md btn-primary text-white"}
                          >
                            Xác nhận
                          </button>
                        </Form>
                      )}
                    </Formik>
                  </div>
                  {/* <div className="card-body">
                    <form
                      method="POST"
                      onSubmit={(e) => {
                        e.preventDefault();
                        console.log(e);
                        // addCourse({ variables: { input: e.target } });
                      }}
                    >
                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <div className="input-group input-group-merge">
                              <div className="input-group-prepend">
                                <span className="input-group-text">
                                  <i className="fas fa-user"></i>
                                </span>
                              </div>
                              <input
                                className="form-control"
                                placeholder="Nhập tên khóa học"
                                type="text"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <a
                        type="submit"
                        className={"btn btn-md btn-primary text-white"}
                      >
                        Xác nhận
                      </a>
                    </form>
                  </div> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
export default CreateCourse;
