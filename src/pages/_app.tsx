import React from "react";
import { HttpLink } from "apollo-link-http";
import { ApolloProvider } from "@apollo/react-hooks";
import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { withApollo } from "next-with-apollo";
import "bootstrap/dist/css/bootstrap.min.css";
import DefaultLayout from "../layouts/MainLayout";
const link = new HttpLink({ uri: "http://localhost:2106/graphql" });

const App = ({ Component, pageProps, apollo }) => {
  const MainLayout = Component.Layout || DefaultLayout;
  return (
    <ApolloProvider client={apollo}>
      <MainLayout>
        <Component {...pageProps} />
      </MainLayout>
    </ApolloProvider>
  );
};

export default withApollo(
  ({ initialState }) =>
    new ApolloClient({
      link: link,
      cache: new InMemoryCache().restore(initialState || {}),
      //  rehydrate the cache using the initial data passed from the server:
    })
)(App);
