import React from "react";
import Head from "next/head";
import TopNav from "../layouts/TopNav";
import Breadcrumb from "../layouts/Breadcrumb";
// import { url } from "inspector";
// import styles from "./style.scss";
import classnames from "classnames";
const styles = {
  backgroundImage: "url(" + "/img/404.png" + ")",
  width: "100%",
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center center"
};
const Custom404 = () => {
  return (
    <React.Fragment>
      <Head>
        <title>404 Not Found | YC Examedu</title>
      </Head>
      <div className="main-content" id="panel">
        <TopNav />
        <div className="header bg-primary pb-6">
          <div className="container-fluid">
            <div className="header-body">
              <Breadcrumb />
            </div>
          </div>
        </div>
        <div>
          <div
            className="row col-12"
            style={{ height: "100vh", margin: "auto" }}
          >
            <div style={styles} />
          </div>
        </div>
      </div>
      <style jsx>{`
        .404__img {
          color: white;
          background: red;
          heigh: 400px;
        }
      `}</style>
      ;
    </React.Fragment>
  );
};

export default Custom404;
