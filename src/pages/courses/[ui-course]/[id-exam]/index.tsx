import React from "react";
import Head from "next/head";
import TopNav from "../../,,/../../../layouts/TopNav";
import Breadcrumb from "../../,,/../../../layouts/Breadcrumb";
import { Row, Col, Card } from "react-bootstrap";
import styles from "./styles.scss";
const Exam = () => {
  let array = [0, 1, 2, 3, 4];
  return (
    <React.Fragment>
      <Head>
        <title>Bài thi số 1</title>
      </Head>
      <div className="main-content" id="panel">
        <TopNav />
        <div className="header bg-primary pb-6">
          <div className="container-fluid">
            <div className="header-body">
              <Breadcrumb />
            </div>
          </div>
        </div>
        <div className="container-fluid mt--6">
          <Row>
            <Col md={8}>
              <div className="card-wrapper">
                {array.map((e, i) => (
                  <Card key={i}>
                    <div className="card-header">
                      <h5 className="h3 mb-0">Câu {i + 1}</h5>
                    </div>
                    <div className="card-body">
                      <p className="card-text mb-4">Ai là người thương em?</p>
                      <div className="custom-control custom-radio mb-3">
                        <input
                          name="custom-radio-1"
                          className="custom-control-input"
                          id="customRadio5"
                          type="radio"
                        />

                        <label
                          className="custom-control-label"
                          htmlFor="customRadio5"
                        >
                          Đáp án 1
                        </label>
                      </div>
                      <div className="custom-control custom-radio mb-3">
                        <input
                          name="custom-radio-1"
                          className="custom-control-input"
                          id="customRadio2"
                          type="radio"
                        />

                        <label
                          className="custom-control-label"
                          htmlFor="customRadio2"
                        >
                          Đáp án 2
                        </label>
                      </div>
                    </div>
                  </Card>
                ))}
                <a href="#" className="btn btn-primary">
                  Câu tiếp theo
                </a>
              </div>
            </Col>
            <Col md={4}>
              <div className="card">
                <div className="card-header">
                  <h3 className="mb-0">Danh sách câu hỏi</h3>
                </div>
                <div className="card-body">
                  <form>
                    <Row>
                      <Col xs={12}>
                        <ul className={styles.listQuestion}>
                          <li>
                            <a href="">
                              <span>1-5</span>
                            </a>
                          </li>
                          <li>
                            <a href="">
                              <span>6-10</span>
                            </a>
                          </li>
                          <li>
                            <a href="">
                              <span>11-15</span>
                            </a>
                          </li>
                        </ul>
                      </Col>
                    </Row>
                  </form>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </React.Fragment>
  );
};
export default Exam;
