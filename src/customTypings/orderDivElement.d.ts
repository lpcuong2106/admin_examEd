// declare module "React" {
//   interface DivAtrribute {
//     "aria-valuenow"?: number;
//   }
//   export interface HTMLDivElement extends DivAtrribute {}
// }
declare module "*.svg"

declare module "*.scss" {
  const content: { [className: string]: string };
  export default content;
}

declare module "*.css" {
  const content: {
    [className: string]: string;
  };
  export default content;
}
