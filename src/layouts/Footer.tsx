import React from "react";

const Footer = () => {
  return (
    <footer className="footer pt-0">
      <div className="row align-items-center justify-content-lg-between">
        <div className="col-lg-6">
          <div className="copyright text-center text-lg-left text-muted">
            &copy; 2020
            {new Date().getFullYear() !== 2020
              ? " - " + new Date().getFullYear()
              : null}
            <a
              href="https://www.facebook.com/phucuong2106"
              className="font-weight-bold ml-1"
              target="_blank"
            >
              Cuong Le
            </a>
          </div>
        </div>
        <div className="col-lg-6">
          <ul className="nav nav-footer justify-content-center justify-content-lg-end">
            <li className="nav-item">
              <a
                href="https://www.creative-tim.com/presentation"
                className="nav-link"
                target="_blank"
              >
                About Us
              </a>
            </li>
            <li className="nav-item">
              <a
                href="http://blog.creative-tim.com"
                className="nav-link"
                target="_blank"
              >
                Blog
              </a>
            </li>
            <li className="nav-item">
              <a
                href="https://www.creative-tim.com/license"
                className="nav-link"
                target="_blank"
              >
                License
              </a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
