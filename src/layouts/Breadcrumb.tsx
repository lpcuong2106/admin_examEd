import React from "react";
import { withRouter } from "next/router";
import { compose } from "recompose";
import { matchPath } from "react-router";
import classnames from "classnames";
import Link from "next/link";
// import NotFound from "../components/NotFound";

// A simple component that shows the pathname of the current location
const renderer = ({ breadcrumb, match }) => {
  if (typeof breadcrumb === "function") {
    return breadcrumb({ match });
  }
  return breadcrumb;
};
const routes = [
  { path: "/create-course", breadcrumb: "Tạo Khóa Học" },
  { path: "/create-course/:pizzaId", breadcrumb: "Edit khóa học" },
  { path: "/pizza/:pizzaId/toppings", breadcrumb: "Pizza Topping" },
  { path: "/another", breadcrumb: "Another" },
  { path: "/one", breadcrumb: "One" },
  { path: "/404", breadcrumb: "404 Not Found" },
];

export const getBreadcrumbs = ({ routes, pathname }) => {
  const matches: any[] = [];
  // console.log(
  //   pathname
  //     .replace(/\/$/)
  //     .split("/")
  //     .reduce((previous, current) => {
  //       console.log(previous);
  //       console.log(current);
  //     })
  // );
  pathname
    .replace(/\/$/)
    .split("/")
    .reduce((previous, current) => {
      const pathSection = `${previous}/${current}`;
      let breadcrumbMatch: any = null;

      routes.some(({ breadcrumb, path }) => {
        const match = matchPath(pathSection, { exact: true, path });
        console.log(pathSection);
        if (match) {
          breadcrumbMatch = {
            breadcrumb: renderer({ breadcrumb, match }),
            path,
            match,
          };
          return true;
        }

        return false;
      });
      if (breadcrumbMatch) {
        matches.push(breadcrumbMatch);
      }

      return pathSection;
    });
  return matches;
};

const Breadcrumb = (props) => {
  // console.group("BreadcrumbComponent");
  // console.log(props);
  const breadcrumbs = getBreadcrumbs({
    pathname: props.router.pathname,
    routes,
  });
  // console.log(breadcrumbs);
  // console.groupEnd();
  return (
    <div className="row align-items-center py-4">
      <div className="col-lg-6 col-7">
        <h6 className="h2 text-white d-inline-block mb-0">Default</h6>
        {/* <NotFound /> */}
        <nav
          aria-label="breadcrumb"
          className="d-none d-md-inline-block ml-md-4"
        >
          <ol className="breadcrumb breadcrumb-links breadcrumb-dark">
            <li className="breadcrumb-item">
              <Link href="/">
                <a>
                  <i className="fas fa-home"></i>
                </a>
              </Link>
            </li>
            {breadcrumbs.map(({ breadcrumb, path, match }, index) => {
              return (
                <li
                  key={index}
                  className={classnames(`breadcrumb-item match`, {
                    active: match,
                  })}
                >
                  <a href={path}>{breadcrumb}</a>
                </li>
              );
            })}
          </ol>
        </nav>
      </div>
      <div className="col-lg-6 col-5 text-right">
        <a href="#" className="btn btn-sm btn-neutral">
          New
        </a>
        <a href="#" className="btn btn-sm btn-neutral">
          Filters
        </a>
      </div>
    </div>
  );
};

export default compose(withRouter(Breadcrumb));
