import React from "react";
import Header from "./header";

const MainLayout = ({ children }) => {
  return (
    <div className="main-container">
      <div className="content-wrapper">
        <Header />
        {children}
      </div>
    </div>
  );
};
export default MainLayout;
