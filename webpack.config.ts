var ExtractTextPlugin = require("extract-text-webpack-plugin");
const StaticSiteGeneratorPlugin = require("static-site-generator-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
var locals = {
  routes: ["/"],
};
module.exports = {
  mode: "development",
  devtool: "inline-source-map",
  entry: {
    main: "./src",
  },
  externals: {
    react: "React",
  },
  // output: {
  //   path: "build",
  //   filename: "bundle.js",
  //   libraryTarget: "umd",
  // },
  module: {
    loaders: [
      {
        test: /\.{tsx|ts|js|jsx}/,
        use: {
          loader: "babel-loader",
        },
        include: __dirname + "/src",
        exclude: /node_modules/,
      },
      {
        test: /,css/,
        loaders: ExtractTextPlugin.extract(
          "css?modules&importLoaders=1&localIdentName=[name]__[local]__[hash:base64:5]"
        ),
        include: __dirname + "/src",
      },
    ],
  },
  plugins: [
    new StaticSiteGeneratorPlugin("main", locals.routes),
    new ExtractTextPlugin("styles.css"),
    new HtmlWebpackPlugin({
      template: "./public/index.html",
      filename: "./index.html",
    }),
  ],
};
//test đã bỏ k dùng
